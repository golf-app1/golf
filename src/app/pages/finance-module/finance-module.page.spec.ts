import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FinanceModulePage } from './finance-module.page';

describe('FinanceModulePage', () => {
  let component: FinanceModulePage;
  let fixture: ComponentFixture<FinanceModulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceModulePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FinanceModulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
