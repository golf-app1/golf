import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FinBudgetPage } from './fin-budget.page';

describe('FinBudgetPage', () => {
  let component: FinBudgetPage;
  let fixture: ComponentFixture<FinBudgetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinBudgetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FinBudgetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
