import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinBudgetPageRoutingModule } from './fin-budget-routing.module';

import { FinBudgetPage } from './fin-budget.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinBudgetPageRoutingModule
  ],
  declarations: [FinBudgetPage]
})
export class FinBudgetPageModule {}
