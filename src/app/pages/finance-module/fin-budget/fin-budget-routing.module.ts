import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinBudgetPage } from './fin-budget.page';

const routes: Routes = [
  {
    path: '',
    component: FinBudgetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinBudgetPageRoutingModule {}
