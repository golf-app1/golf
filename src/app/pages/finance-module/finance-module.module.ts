import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinanceModulePageRoutingModule } from './finance-module-routing.module';

import { FinanceModulePage } from './finance-module.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinanceModulePageRoutingModule,
  ],
  //declarations: [FinanceModulePage]
})
export class FinanceModulePageModule {}
