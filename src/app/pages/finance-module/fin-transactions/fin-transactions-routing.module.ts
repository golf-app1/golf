import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinTransactionsPage } from './fin-transactions.page';

const routes: Routes = [
  {
    path: '',
    component: FinTransactionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinTransactionsPageRoutingModule {}
