import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinTransactionsPageRoutingModule } from './fin-transactions-routing.module';

import { FinTransactionsPage } from './fin-transactions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinTransactionsPageRoutingModule
  ],
  declarations: [FinTransactionsPage]
})
export class FinTransactionsPageModule {}
