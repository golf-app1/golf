import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinDashboardPage } from './fin-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: FinDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinDashboardPageRoutingModule {}
