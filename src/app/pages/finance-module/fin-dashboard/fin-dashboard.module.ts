import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinDashboardPageRoutingModule } from './fin-dashboard-routing.module';

import { FinDashboardPage } from './fin-dashboard.page';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { GoalBudgetComponent } from './goal-budget/goal-budget.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinDashboardPageRoutingModule,
    Ng2GoogleChartsModule
  ],
  declarations: [
    //FinDashboardPage,
    //GoalBudgetComponent
  ]
})
export class FinDashboardPageModule {}
