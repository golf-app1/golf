import { Component, OnInit } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts';

@Component({
  selector: 'app-fin-dashboard',
  templateUrl: './fin-dashboard.page.html',
  styleUrls: ['./fin-dashboard.page.scss'],
})
export class FinDashboardPage implements OnInit {

  constructor() {

   }
   public pieChart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [
      ['Category', 'Period Expense'],
      ['Drinks',     1000],
      ['Meals',      2000],
      ['Club',  10000],
      ['Kit', 15000],
      ['Uncategorized',    7000]
    ],
    //firstRowIsData: true,
    options: {
     // 'title': 'Tasks',
      width: '200',
      height:'200',
      pieHole: 0.5,
      legend: 'none'
    },
  };

  ngOnInit() {
  }

}
