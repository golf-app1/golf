import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'goal-budget',
  templateUrl: './goal-budget.component.html',
  styleUrls: ['./goal-budget.component.scss'],
})
export class GoalBudgetComponent implements OnInit {

  @Input() totalBudget: any;
  @Input() balance:any;
  @Input() utilizedAmount:any;
  @Input() icon:any;
  @Input() expenseCategory:any;
  @Input() pctUtilization:any;


  constructor() { }

  ngOnInit() {}

}
