import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinanceModulePage } from './finance-module.page';

const routes: Routes = [
  {
    path: '',
    component: FinanceModulePage,
    children: [
      {
        path: 'fin-dashboard',
        loadChildren: () => import('./fin-dashboard/fin-dashboard.module').then( m => m.FinDashboardPageModule)
      },
      {
        path: 'fin-transactions',
        loadChildren: () => import('./fin-transactions/fin-transactions.module').then( m => m.FinTransactionsPageModule)
      },
      {
        path: 'fin-budget',
        loadChildren: () => import('./fin-budget/fin-budget.module').then( m => m.FinBudgetPageModule)
      },
      {
        path: '',
        redirectTo: '/fin-dashboard',
        pathMatch: 'full'
      }

    ]
  },
  {
    path: '',
    redirectTo: '/fin-dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinanceModulePageRoutingModule {}
