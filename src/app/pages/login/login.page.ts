import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html'
  //styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  login: FormGroup;
  loading: any;

  constructor(private route:Router,
    public loadingCtrl: LoadingController) { 

      this.login = new FormGroup({
        Username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required)
      });
    }

  ngOnInit() {
  }

  doLogin(){
    console.log(this.login.value);
    this.route.navigate(['finance-module','fin-dashboard']);
  }
  forgetPassword(){
    this.route.navigate(['forgot-password']);
  }

}
