import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';

// Ionic Native Plugins
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AdMobFree } from '@ionic-native/admob-free/ngx';
import { AppRate } from '@ionic-native/app-rate/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { Ng2GoogleChartsModule , GoogleChartsSettings } from 'ng2-google-charts';
//Pages
import { LoginPage } from './pages/login/login.page';
import { FinanceModulePage } from './pages/finance-module/finance-module.page';
import { FinDashboardPage } from './pages/finance-module/fin-dashboard/fin-dashboard.page';
import { GoalBudgetComponent } from './pages/finance-module/fin-dashboard/goal-budget/goal-budget.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    FinanceModulePage,
    FinDashboardPage,
    GoalBudgetComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule, 
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(),
		TranslateModule.forRoot({
    loader: {
				provide: TranslateLoader,
				useFactory: (createTranslateLoader),
				deps: [HttpClient]
			}
    }), 
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2GoogleChartsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    
    //ionic native plugins
	  SplashScreen,
	  StatusBar,
    SocialSharing,
    NativeStorage,
    InAppBrowser,
    Keyboard,
    Geolocation,
		AdMobFree,
		AppRate,
		ImagePicker,
		Crop,
		EmailComposer,
    { provide: RouteReuseStrategy, 
      useClass: IonicRouteStrategy 
    }
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
